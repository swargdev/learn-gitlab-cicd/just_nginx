# CI/CD for Simple static site

- Single container application consisting of a nginx web-server
 with one index.html file, configured to work with localhost domain.

- CI/CD with auto deploy to dev-stage server on any commit

when the `test` success passed the built docker-image is push to the
GitLab Registry and pulled on `deploy to dev` stage.

Depends: Two tagged GitLab-Runners:
`docker` - to build and test dockerized application
`dev-shell` - to deploy to dev-stage server


