FROM alpine:3.17.3

RUN apk add --no-cache nginx && mkdir -p /run/nginx

EXPOSE 80

# COPY custom.conf /etc/nginx/conf.d/
COPY custom.conf /etc/nginx/http.d/
COPY . /opt/

CMD ["nginx", "-g", "daemon off;"]


