#!/bin/bash

echo -en "\033[37;1;42m Check start page: \033[0m"

# is 200
curl -H "Host: localhost" --silent --show-error --fail -I http://localhost/
echo -e "--------\n"

# Show line <h1>I am Nginx!</h1>
curl --silent --show-error --fail http://localhost/ | grep '<h1>'


